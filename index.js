const audios = {
    87: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/SD0025.mp3',
    69: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/SD0000.mp3',
    82: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/SD0000.mp3',
    84: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/RS.mp3',
    83: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/OH25.mp3',
    68: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/MA.mp3',
    70: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/CY0010.mp3',
    71: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/CH.mp3',
    90: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/CB.mp3',
    88: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/BD0010.mp3',
    67: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/BD0000.mp3',
    86: 'http://www.disasterdoug.com/S-SV_files/alt_stand_training/Module1/Resources/Sounds/drum.wav',
}

const play = keyCode => (new Audio(audios[keyCode])).play();
document.onkeydown = e => play(e.keyCode);

const template = Object.keys(audios)
    .map(keyCode => {
        const keyName = String.fromCharCode(keyCode);
        return `<div class="col-3 m-1 d-flex justify-content-center align-items-center" onclick="play('${ keyCode }')">Press ${ keyName }</div>`;
    })
    .join('');

document.querySelector('.container').innerHTML = `<div class="row mx-auto">${ template }</div>`;
